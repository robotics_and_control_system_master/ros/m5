import time
import rospy
import geometry_msgs.msg
from gazebo_msgs.srv import DeleteModel, SpawnModel


rospy.init_node("node_spawn_gazebo")

model_name = "my_box1"

goal_position = geometry_msgs.msg.Pose()
goal_position.position.x = 0.5
goal_position.position.y = 0.5
goal_position.position.z = 0.5

# verify spawn service is active and connect to the service proxy
rospy.wait_for_service("gazebo/spawn_sdf_model")

spawn_model_prox = rospy.ServiceProxy('gazebo/spawn_sdf_model', SpawnModel)

# read object model from SDF file
with open("/home/ros/mu/m5_ws/src/models/my_box/model.sdf", "r") as f:
    model = f.read()
    
    # spawn the model in the world coordinates
    spawn_model_prox(model_name, model, '', goal_position, "world")

time.sleep(5)

# if you are interested to delete you can 
rospy.wait_for_service("gazebo/delete_model")
del_model_prox = rospy.ServiceProxy("gazebo/delete_model", DeleteModel)
del_model_prox(model_name)
